# NURE Cinema Documentation

## Use-Case scenarios
![use-case](diagrams/img/nure-cinema-use-case.jpg)

## Database ERD
![database-erd](diagrams/img/nure-cinema-database.png)

## System-Context diagram
![context-diagram](diagrams/img/nure-cinema-context.jpg)

## Container diagram
![container-diagram](diagrams/img/nure-cinema-container.jpg)

## Mobile app wireframe
![wireframe](diagrams/img/nure-cinema-wireframe.png)
